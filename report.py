#!/usr/bin/env python3

import os
import time
import re
import getpass
import sys
from jinja2 import Environment

HERE = os.path.dirname(os.path.abspath(__file__))

def main():
    if not os.path.isfile(HERE + '/email.conf'):
        print('email.conf not found in script\'s directory')
        print('Please put your list of client emails in email.conf')
        sys.exit()

    with open(HERE + '/email.conf') as f:
        emails = f.readlines()

    emails = [ email[:-1] for email in emails if re.match('[^@]+@[^@]+\.[^@]+', email) ]

    user = input("Your email address > ")
    while not re.match('[^@]+@zies\.net', user):
        print("Incorrect email address format, try again.")
        user = input("Your email address > ")

    password = getpass.getpass("Your email password > ")

    print()
    print("[INFO] Email subject can't start with numbers, this is a limitation")
    print("from the database.")
    subject = input("Email subject > ")

    print('Sending email to: {total} address(es)'.format(total=len(emails)))
    print('From: {user}'.format(user=user))
    execute = input('Continue? (y/N) > ')
    if execute.lower() == 'y':
        sendmail(user, subject, user, password)
        print('Sent an email to {user} for testing, please check'.format(user=user))
        print('if the content is correct.')

        execute = input('Blast to everyone? (y/N) > ')
        if execute.lower() == 'y':
            total = len(emails)
            i = 1
            for email in emails:
                print('Sending {i}/{total}'.format(i=i, total=total), end='\r')
                sendmail(email, subject, user, password)
                i += 1

def writedb(email, subject):
    import sqlite3

    sqlitedb = HERE + '/email.db'
    if not os.path.isfile(sqlitedb):
        os.system('touch \'{}\''.format(sqlitedb))
        conn = sqlite3.connect(sqlitedb)
        c = conn.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS history (email TEXT PRIMARY KEY)")
        conn.commit()
    else:
        conn = sqlite3.connect(sqlitedb)
        c = conn.cursor()

    c.execute("PRAGMA table_info(history)")
    columns = c.fetchall()

    clean_subj = ''.join(e for e in subject if e.isalnum())
    if not any("{}".format(clean_subj) in col for col in columns):
        c.execute("ALTER TABLE history ADD COLUMN {subject} INTEGER".format(subject=clean_subj))
        conn.commit()

    c.execute("INSERT OR IGNORE INTO history (email) VALUES (\'{email}\')".format(email=email))
    conn.commit()

    c.execute("SELECT {subject} FROM history WHERE email=\'{email}\'".format(email=email, subject=clean_subj))
    record = c.fetchone()
    if record[0] is None: 
        c.execute("UPDATE history SET {subject} = 1 WHERE email = \'{email}\'".format(email=email, subject=clean_subj))
        conn.commit()
        conn.close()
        return False

    conn.close()
    return True

def sendmail(email, subject, user, password):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    import smtplib

    gmail_user = user
    gmail_pwd  = password
    recipient = email

    if not os.path.isfile(HERE + '/email.html'):
        print('email.html not found is script\'s directory')
        print('Please put your email.html to the script\'s directory')
        sys.exit()

    with open(HERE + '/email.html', 'r') as f:
        message = f.readlines()

    tracking_subject = ''.join(e.lower() for e in subject if e.isalnum())

    message = " ".join(message)
    message = Environment().from_string(message).render(
            user=user,
            subject=tracking_subject
            )

    msg = MIMEMultipart()
    msg['From'] = gmail_user
    msg['Subject'] = subject
    msg.attach(MIMEText(message, 'html'))

    record_exist = writedb(recipient, tracking_subject)
    if not record_exist:
        mail_server = smtplib.SMTP('smtp.gmail.com', 587)
        mail_server.ehlo()
        mail_server.starttls()
        mail_server.ehlo()
        mail_server.login(gmail_user, gmail_pwd)
        msg['To'] = recipient
        mail_server.sendmail(gmail_user, recipient, msg.as_string())
        mail_server.close()

if __name__ == '__main__':
    main()
